package com.rabbitMQ;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang.SerializationUtils;

import common.DVD;
import rabbitMQ.RabbitMQEndPoint;


/**
 * The producer endpoint that writes to the queue.
 * @author syntx
 *
 */
public class Producer extends RabbitMQEndPoint {

    public Producer(String endPointName) throws IOException, TimeoutException{
        super(endPointName);
    }

    public boolean sendDvd(DVD dvd) {
        try {
            channel.basicPublish("",endPointName, null, SerializationUtils.serialize(dvd));
            return true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }
}
