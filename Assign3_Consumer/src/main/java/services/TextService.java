package services;

import common.DVD;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class TextService {
	
	DVD dvd;
	
	
	public TextService(DVD dvd) throws FileNotFoundException
	{
		this.dvd = dvd;
		
		PrintWriter out = new PrintWriter("DvdFile.txt");
		out.println(dvd);
		out.close();
		System.out.println("Text file created successfully.");
	}


}
