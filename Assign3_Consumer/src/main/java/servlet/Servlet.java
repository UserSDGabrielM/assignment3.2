package servlet;

import com.rabbitMQ.Producer;
import common.DVD;
import rabbitMQ.QueueConsumer;
import services.TextService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Servlet extends HttpServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        RequestDispatcher view = request.getRequestDispatcher("/index.xhtml");
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException{
        String artist = request.getParameter("artist");
        String album = request.getParameter("album");
        Integer year = Integer.parseInt(request.getParameter("year"));
        Double price = Double.parseDouble(request.getParameter("price"));

        DVD dvd = new DVD(artist, album, price, year);

        try{
            QueueConsumer elem = new QueueConsumer("queue");
            Thread consumerThread = new Thread(elem);
            consumerThread.start();
            Producer producer = new Producer("queue");
            TextService textService = new TextService(dvd);
            producer.sendDvd(dvd);
        } catch(IOException |TimeoutException e ){
            e.printStackTrace();
        }
    }
}
