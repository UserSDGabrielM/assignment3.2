package rabbitMQ;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import common.DVD;
import org.apache.commons.lang.SerializationUtils;
import services.EmailService;
import user.User;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


/**
 * The endpoint that consumes messages off of the queue. Happens to be runnable.
 * @author syntx
 *
 */
public class QueueConsumer extends RabbitMQEndPoint implements Runnable, Consumer{
	
	public EmailService mailService;

	public User user = new User();

	public QueueConsumer(String endPointName) throws IOException, TimeoutException{
		super(endPointName);	
		mailService = new EmailService(user.getUsername(), user.getPassword());
	}
	
	public void run() {
		try {
			//start consuming messages. Auto acknowledge messages.
			channel.basicConsume(endPointName, true,this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Called when consumer is registered.
	 */
	public void handleConsumeOk(String consumerTag) {
		System.out.println("Consumer "+consumerTag +" registered");		
	}

	/**
	 * Called when new message is available.
	 */
	public void handleDelivery(String consumerTag, Envelope env,
			BasicProperties props, byte[] body) throws IOException {
		DVD dvd = (DVD) SerializationUtils.deserialize(body);
		if (dvd != null) {
			
			String dvdString = "DVD: " + "Artist: " + dvd.getArtist() + "  Album: " + dvd.getAlbumName() + "  Price: " + dvd.getPrice() + "$  Year: " + dvd.getYear() + "\n";
			System.out.println( dvdString + "has been received from producer.\n");
			System.out.println("Sending mail... \n");
			mailService.sendMail("gc.marinescu@yahoo.ro","New DVD added!",dvdString);
			System.out.println("Mail successfully sent! \n");
		}
	    
		
	}

	public void handleCancel(String consumerTag) {}
	public void handleCancelOk(String consumerTag) {}
	public void handleRecoverOk(String consumerTag) {}
	public void handleShutdownSignal(String consumerTag, ShutdownSignalException arg1) {}

	/*
	public static void main(String[] args) throws IOException, TimeoutException {
		String artist, album;
		Double price;
		Integer year;
		TextService txt;
		
		QueueConsumer elem = new QueueConsumer("queue");
		Thread consumerThread = new Thread(elem);
		consumerThread.start();
		Producer producer = new Producer("queue");
		Scanner scan = new Scanner(System.in);		
		
		System.out.println("DVD artist: ");
		artist = scan.nextLine();
		System.out.println("DVD album: ");
		album = scan.nextLine();
		System.out.println("DVD price: ");
		price = scan.nextDouble();
		System.out.println("DVD year: ");
		year = scan.nextInt();
		
		DVD myDvd = new DVD(artist, album, price, year);
		
		txt= new TextService(myDvd);		
		producer.sendDvd(myDvd);
		
		System.out.println("The DVD: " + myDvd.toString()+" has been sent.");
		}
		*/
	}

